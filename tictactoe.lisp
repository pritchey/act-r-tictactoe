(defvar *board* nil)

(defun reset-board ()
	(setq *board* (make-array '(3 3) :element-type 'character))
	
	(dotimes (r 3)
		(dotimes (c 3)
			(setf (aref *board* r c) #\Space)
		)
	)
)

(defun update-board (r c m)
	(setf (aref *board* (- r 1) (- c 1)) m)
)

(defun win-check ()
	(setf w nil)
	(dotimes (i 3)
		(if (or 
				(and
					(not (string= (aref *board* i 0) #\Space))
					(string= (aref *board* i 0) (aref *board* i 1))
					(string= (aref *board* i 0) (aref *board* i 2))
				)
				(and
					(not (string= (aref *board* 0 i) #\Space))
					(string= (aref *board* 0 i) (aref *board* 1 i))
					(string= (aref *board* 0 i) (aref *board* 2 i))
				)
			)
			(setf w t)
		)
	)
	(if (not w)
		(if (or
				(and
					(not (string= (aref *board* 0 0) #\Space))
					(string= (aref *board* 0 0) (aref *board* 1 1))
					(string= (aref *board* 0 0) (aref *board* 2 2))
				)
				(and
					(not (string= (aref *board* 0 2) #\Space))
					(string= (aref *board* 0 2) (aref *board* 1 1))
					(string= (aref *board* 0 2) (aref *board* 2 0))
				)
			)
			(setf w t)
		)
	)
	(if w (handle-win) (handle-no-win))
)

(defun handle-win ()
	(print 'win)
	
	(print "again? (y/n) ")
	(setf c (char (read-line) 0))
	(if (or (string= c #\y) 
			(string= c #\Y))
		"continue-win"
		"stop-win"
	)
)

(defun handle-no-win ()
	(setf d t)
	(dotimes (r 3)
		(dotimes (c 3)
			(if (string= (aref *board* r c) #\Space) (setf d nil))
		)
	)
	(if d (handle-draw) (handle-unfinished))
)

(defun handle-draw ()
	(print 'draw)
	(print "new game? (y/n) ")
	(setf c (char (read-line) 0))
	(if (or (string= c #\y) 
			(string= c #\Y))
		"continue-draw"
		"stop-draw"
	)
)

(defun handle-unfinished ()
	"prep-next-move"
)

(defun print-board ()
	(with-output-to-string (s)
		(write-char #\linefeed s)
		(write-char #\Space s)
		(write-char (aref *board* 0 0) s)
		(write-char #\Space s)
		(write-char #\| s)
		(write-char #\Space s)
		(write-char (aref *board* 0 1) s)
		(write-char #\Space s)
		(write-char #\| s)
		(write-char #\Space s)
		(write-char (aref *board* 0 2) s)
		(write-char #\linefeed s)
		(write-line "---+---+---" s)
		(write-char #\Space s)
		(write-char (aref *board* 1 0) s)
		(write-char #\Space s)
		(write-char #\| s)
		(write-char #\Space s)
		(write-char (aref *board* 1 1) s)
		(write-char #\Space s)
		(write-char #\| s)
		(write-char #\Space s)
		(write-char (aref *board* 1 2) s)
		(write-char #\linefeed s)
		(write-line "---+---+---" s)
		(write-char #\Space s)
		(write-char (aref *board* 2 0) s)
		(write-char #\Space s)
		(write-char #\| s)
		(write-char #\Space s)
		(write-char (aref *board* 2 1) s)
		(write-char #\Space s)
		(write-char #\| s)
		(write-char #\Space s)
		(write-char (aref *board* 2 2) s)
		(write-char #\linefeed s))
)

(defun get-human-input ()
	(print (print-board))
	(print "your move: ")

	(setf c (read-line))
	(setf n (- (char-code (char c 0)) (char-code #\0)))
	(setf row (+ 1 (floor (/ (- n 1) 3))))
	(setf col (+ 1 (mod (- n 1) 3)))
	
	(if (or
			(< n 1)
			(> n 9)
			(not (string= (aref *board* (- row 1) (- col 1)) #\Space))
		)
		(get-human-input)
		(values row col)
	)
)

(defun get-board-state ()
	(values 
		(aref *board* 0 0)
		(aref *board* 0 1)
		(aref *board* 0 2)
		(aref *board* 1 0)
		(aref *board* 1 1)
		(aref *board* 1 2)
		(aref *board* 2 0)
		(aref *board* 2 1)
		(aref *board* 2 2)
	)
)

(clear-all)

(define-model tictactoe

(sgp 
	:er		t		; enable randomness parameter
	:esc 	t 		; enable symbolic computations
	:lf 	.05		; letency factor
	:ul 	t		; utility learning flag
	:ult 	nil		; utility learning trace flag
	:bll	0.5		; base-level learning decay parameter (turns bll on)
	:ol		t		; optimized learning
	:act	nil		; activation trace parameter
	:rt		-2		; retrieval threshold parameter
	:trace-detail low
	:declarative-num-finsts 14 
	:declarative-finst-span 10
)

(chunk-type cell row col mark)
(chunk-type player-marker player mark)
(chunk-type task task-name param1 param2 param3 cp tl tm tr ml mm mr bl bm br)
(chunk-type board-move tl tm tr ml mm mr bl bm br row col)
(chunk-type	cell-location row col loc)
(chunk-type other-player p1 p2)

(add-dm
	(tl isa cell row 1 col 1 mark 'open)
	(tm isa cell row 1 col 2 mark 'open)
	(tr isa cell row 1 col 3 mark 'open)
	(ml isa cell row 2 col 1 mark 'open)
	(mm isa cell row 2 col 2 mark 'open)
	(mr isa cell row 2 col 3 mark 'open)
	(bl isa cell row 3 col 1 mark 'open)
	(bm isa cell row 3 col 2 mark 'open)
	(br isa cell row 3 col 3 mark 'open)
	(p1-marker isa player-marker player 1 mark #\X)
	(p2-marker isa player-marker player 2 mark #\O)
	(my-mark isa player-marker player 'me)
	(human-mark isa player-marker player 'human) 
	(op1 isa other-player p1 'me p2 'human)
	(op2 isa other-player p1 'human p2 'me)
	(s isa board-move tl 'open tm 'open tr 'open ml 'open mm 'open mr 'open bl 'open bm 'open br 'open)
	(loc-tl isa cell-location row 1 col 1 loc 'tl)
	(loc-tm isa cell-location row 1 col 2 loc 'tm)
	(loc-tr isa cell-location row 1 col 3 loc 'tr)
	(loc-ml isa cell-location row 2 col 1 loc 'ml)
	(loc-mm isa cell-location row 2 col 2 loc 'mm)
	(loc-mr isa cell-location row 2 col 3 loc 'mr)
	(loc-bl isa cell-location row 3 col 1 loc 'bl)
	(loc-bm isa cell-location row 3 col 2 loc 'bm)
	(loc-br isa cell-location row 3 col 3 loc 'br)
	(begin isa task task-name 'determine-my-mark)
 )

(P start "start by figuring out which players we are"
	=goal>
		isa			task
		task-name	'determine-my-mark
==>
	=goal>
		task-name	'determining-my-mark
	+retrieval>
		isa			player-marker
		player		=rand-player
	!eval! (reset-board)
	!bind! =rand-player (+ 1 (random 2))
)

(P determined-my-mark "figured out which player i am"
	=goal>
		isa			task
		task-name	'determining-my-mark
	=retrieval>
		isa			player-marker
		player		=p
		mark		=m
==>
	=goal>
		task-name	'determine-human-mark 
		param1		=m
	+retrieval>
		isa			player-marker
		player		=other-player
	!bind! =other-player (- 3 =p)
)

(P determining-human-mark "figure out which player the human is"
	=goal>
		isa			task
		task-name	'determine-human-mark
	=retrieval>
		isa			player-marker
		mark		=m
==>
	=goal>
		task-name	'set-my-mark
		param2		=m
	+retrieval>
		isa			player-marker
		player		'me
)

(P set-my-mark "remember which player i am"
	=goal>
		isa			task
		task-name	'set-my-mark
		param1		=m
	=retrieval>
		isa			player-marker
		player		'me
==>
	=goal>
		task-name	'prep-set-human-mark
	=retrieval>
		mark		=m
)

(P prepare-to-set-human-mark "get ready to remember which player human is"
	=goal>
		isa			task
		task-name	'prep-set-human-mark
==>
	=goal>
		task-name	'set-human-mark
	+retrieval>
		isa			player-marker
		player		'human
)

(P set-human-mark "remember which player human is"
	=goal>
		isa			task
		task-name	'set-human-mark
		param2		=m
	=retrieval>
		isa			player-marker
		player		'human
==>
	=goal>
		task-name	'pre-start-game
	=retrieval>
		mark		=m
)

(P pre-start-game-me
	=goal>
		isa			task
		task-name	'pre-start-game
		param1		#\X
==>
	=goal>
		task-name	'start-game
		param1		nil
		param2		nil
		cp			'me
)

(P pre-start-game-human
	=goal>
		isa			task
		task-name	'pre-start-game
		param2		#\X
==>
	=goal>
		task-name	'start-game
		param1		nil
		param2		nil
		cp			'human
)

(P start-game "start the game"
	=goal>
		isa			task
		task-name	'start-game
		cp			=cp
==>
	=goal>
		task-name	'next-move
		tl			'open
		tm			'open
		tr			'open
		ml			'open
		mm			'open
		mr			'open
		bl			'open
		bm			'open
		br			'open
	+retrieval>
		isa			player-marker
		player		=cp
		mark		#\X
)

(P prep-next-move
	=goal>
		isa			task
		task-name	"prep-next-move"
		param1		=m
	=retrieval>
		isa			other-player
		p1			=cp
		p2			=np
==>
	=goal>
		task-name	'next-move
	+retrieval>
		isa			player-marker
		player		=np
		- mark		=m
		- mark		nil
)

(P next-move-human "fires if the next move is the human's to make"
	=goal>
		isa			task
		task-name	'next-move
	=retrieval>
		isa			player-marker
		player		'human
		mark		=m
==>
	=goal>
		task-name	'prep-make-mark
		param1		=r
		param2		=c
		cp			'human
	+retrieval>
		isa			player-marker
		player		'human
		mark		=m
	!mv-bind! (=r =c) (get-human-input)	
)

(P next-move-actr "fires if the next move is actr's to make"
	=goal>
		isa			task
		task-name	'next-move
	=retrieval>
		isa			player-marker
		player		'me
		mark		=m
==>
	=goal>
		task-name	'get-board-move
		cp			'me
		param1		=m
)

(P get-board-move "find a board-move pair for current board state"
	=goal>
		isa			task
		task-name	'get-board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
==>
	=goal>
		task-name	'got-board-move
	+retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
		- row		nil
		- col		nil
)

(P got-board-move "found board-move pair"
	=goal>
		isa			task
		task-name	'got-board-move
		param1		=m
	=retrieval>
		isa			board-move
		row			=r
		col			=c
==>
	=goal>
		task-name	'prep-make-mark
		param1		=r
		param2		=c
	+retrieval>
		isa			player-marker
		player		'me
		mark		=m
)

(P not-got-board-move "did not find board-move pair"
	=goal>
		isa			task
		task-name	'got-board-move
	?retrieval>
		state		error
==>
	=goal>
		task-name	'get-open-cell
)

(P get-tl "get the top-left cell"
	=goal>
		isa			task
		task-name	'get-open-cell
		tl			'open
==>
	=goal>
		task-name	'got-open-cell
	+retrieval>
		isa			cell
		row			1
		col			1
		mark		'open
)

(P get-tm "get the top-middle cell"
	=goal>
		isa			task
		task-name	'get-open-cell
		tm			'open
==>
	=goal>
		task-name	'got-open-cell
	+retrieval>
		isa			cell
		row			1
		col			2
		mark		'open
)

(P get-tr "get the top-right cell"
	=goal>
		isa			task
		task-name	'get-open-cell
		tr			'open
==>
	=goal>
		task-name	'got-open-cell
	+retrieval>
		isa			cell
		row			1
		col			3
		mark		'open
)

(P get-ml "get the middle-left cell"
	=goal>
		isa			task
		task-name	'get-open-cell
		ml			'open
==>
	=goal>
		task-name	'got-open-cell
	+retrieval>
		isa			cell
		row			2
		col			1
		mark		'open
)

(P get-mm "get the middle-middle cell"
	=goal>
		isa			task
		task-name	'get-open-cell
		mm			'open
==>
	=goal>
		task-name	'got-open-cell
	+retrieval>
		isa			cell
		row			2
		col			2
		mark		'open
)

(P get-mr "get the middle-right cell"
	=goal>
		isa			task
		task-name	'get-open-cell
		mr			'open
==>
	=goal>
		task-name	'got-open-cell
	+retrieval>
		isa			cell
		row			2
		col			3
		mark		'open
)

(P get-bl "get the bottom-left cell"
	=goal>
		isa			task
		task-name	'get-open-cell
		bl			'open
==>
	=goal>
		task-name	'got-open-cell
	+retrieval>
		isa			cell
		row			3
		col			1
		mark		'open
)

(P get-bm "get the bottom-middle cell"
	=goal>
		isa			task
		task-name	'get-open-cell
		bm			'open
==>
	=goal>
		task-name	'got-open-cell
	+retrieval>
		isa			cell
		row			3
		col			2
		mark		'open
)

(P get-br "get the bottom-right cell"
	=goal>
		isa			task
		task-name	'get-open-cell
		br			'open
==>
	=goal>
		task-name	'got-open-cell
	+retrieval>
		isa			cell
		row			3
		col			3
		mark		'open
)

(P got-open-cell "retrieved an open cell"
	=goal>
		isa			task
		task-name	'got-open-cell
		param1		=m
	=retrieval>
		isa			cell
		row			=r
		col			=c
==>
	=goal>
		task-name	'prep-make-mark
		param1		=r
		param2		=c
	+retrieval>
		isa			player-marker
		player		'me
		mark		=m
)

(P prep-make-mark "figure out where and what to mark"
	=goal>
		isa			task
		task-name	'prep-make-mark
		param1		=r
		param2		=c
	=retrieval>
		isa			player-marker
		mark		=m
==>
	=goal>
		task-name	'make-mark
		param1		=m
		param2		nil
	+retrieval>
		isa			cell
		row			=r
		col			=c
		mark		'open
)

(P make-mark "actually make the mark on the board"
	=goal>
		isa			task
		task-name	'make-mark
		param1		=m
	=retrieval>
		isa			cell
		row			=r
		col			=c
		mark		'open
==>
	=goal>
		task-name	'make-mark2
		param1		=r
		param2		=c
		param3		=m
	=retrieval>
		mark		=m
	!eval! (update-board =r =c =m)
)

(P make-mark2 "update board-move pair"
	=goal>
		isa			task
		task-name	'make-mark2
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
==>
	=goal>
		task-name	'make-mark3
	+retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
)

; don't save act-r's choices
(P make-mark3-me
	=goal>
		isa			task
		task-name	'make-mark3
		cp			'me
==>
	=goal>
		task-name	'make-mark4
)

; only save human moves
(P make-mark3-human
	=goal>
		isa			task
		task-name	'make-mark3
		param1		=r
		param2		=c
		cp			'human
	=retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
==>
	=goal>
		task-name	'make-mark4
	=retrieval>
		row			=r
		col			=c
)
	

(P make-mark4
	=goal>
		isa			task
		task-name	'make-mark4
		param1		=r
		param2		=c
==>
	=goal>
		task-name	'make-mark5
	+retrieval>
		isa			cell-location
		row			=r
		col			=c
)

(P make-mark5-tl
	=goal>
		isa			task
		task-name	'make-mark5
		param3		=m
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
	=retrieval>
		isa			cell-location
		loc			'tl
==>
	=goal>
		task-name	'make-mark6
		tl			=m
	+retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
)

(P make-mark5-tm
	=goal>
		isa			task
		task-name	'make-mark5
		param3		=m
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
	=retrieval>
		isa			cell-location
		loc			'tm
==>
	=goal>
		task-name	'make-mark6
		tm			=m
	+retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
)

(P make-mark5-tr
	=goal>
		isa			task
		task-name	'make-mark5
		param3		=m
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
	=retrieval>
		isa			cell-location
		loc			'tr
==>
	=goal>
		task-name	'make-mark6
		tr			=m
	+retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
)

(P make-mark5-ml
	=goal>
		isa			task
		task-name	'make-mark5
		param3		=m
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
	=retrieval>
		isa			cell-location
		loc			'ml
==>
	=goal>
		task-name	'make-mark6
		ml			=m
	+retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
)

(P make-mark5-mm
	=goal>
		isa			task
		task-name	'make-mark5
		param3		=m
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
	=retrieval>
		isa			cell-location
		loc			'mm
==>
	=goal>
		task-name	'make-mark6
		mm			=m
	+retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
)

(P make-mark5-mr
	=goal>
		isa			task
		task-name	'make-mark5
		param3		=m
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
	=retrieval>
		isa			cell-location
		loc			'mr
==>
	=goal>
		task-name	'make-mark6
		mr			=m
	+retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
)

(P make-mark5-bl
	=goal>
		isa			task
		task-name	'make-mark5
		param3		=m
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
	=retrieval>
		isa			cell-location
		loc			'bl
==>
	=goal>
		task-name	'make-mark6
		bl			=m
	+retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
)

(P make-mark5-bm
	=goal>
		isa			task
		task-name	'make-mark5
		param3		=m
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
	=retrieval>
		isa			cell-location
		loc			'bm
==>
	=goal>
		task-name	'make-mark6
		bm			=m
	+retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
)

(P make-mark5-br
	=goal>
		isa			task
		task-name	'make-mark5
		param3		=m
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
	=retrieval>
		isa			cell-location
		loc			'br
==>
	=goal>
		task-name	'make-mark6
		br			=m
	+retrieval>
		isa			board-move
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
)

(P make-mark6
	=goal>
		isa			task
		task-name	'make-mark6
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
	=retrieval>
		isa			board-move
==>
	=goal>
		task-name	'make-markN
	=retrieval>
		tl			=tl
		tm			=tm
		tr			=tr
		ml			=ml
		mm			=mm
		mr			=mr
		bl			=bl
		bm			=bm
		br			=br
		row			nil
		col			nil
)

(P make-markN
	=goal>
		isa			task
		task-name	'make-markN
		param1		=r
		param2		=c
		param3		=m
==>
	=goal>
		task-name	'win-check
		param1		=m
		param2		nil
		param3		nil
	+retrieval>
		isa			cell
		row			=r
		col			=c
		- mark		'open
)

(P win-check "check for a win"
	=goal>
		isa			task
		task-name	'win-check
		cp			=cp
==>
	=goal>
		task-name	=next-task
	+retrieval>
		isa			other-player
		p1			=cp
	!bind! =next-task (win-check)
)

(P stop-win-me "stop after act-r win"
	=goal>
		isa			task
		task-name	"stop-win"
		cp			'me
==>
	=goal>
		task-name	'stop
)

(P stop-win-human "stop after human win"
	=goal>
		isa			task
		task-name	"stop-win"
		cp			'human
==>
	=goal>
		task-name	'stop
)

(P stop-draw "stop after a draw"
	=goal>
		isa			task
		task-name	"stop-draw"
==>
	=goal>
		task-name	'stop
)

(P continue-draw "continue after a draw"
	=goal>
		isa			task
		task-name	"continue-draw"
==>
	=goal>
		task-name	'determine-my-mark
		param1		nil
)

(P continue-win-me "continue after act-r win"
	=goal>
		isa			task
		task-name	"continue-win"
		cp			'me
==>
	=goal>
		task-name	'determine-my-mark
		param1		nil
)

(P continue-win-human "continue after human win"
	=goal>
		isa			task
		task-name	"continue-win"
		cp			'human
==>
	=goal>
		task-name	'determine-my-mark
		param1		nil
)

(P stop "stop playing"
	=goal>
		isa			task
		task-name	'stop
==>
	-goal>
)

(goal-focus begin)
(spp stop-win-me :reward 2)
(spp continue-win-me :reward 2)
(spp stop-win-human :reward -2)
(spp continue-win-human :reward -2)
(spp stop-draw :reward 1)
(spp continue-draw :reward 1)
)
